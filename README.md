# OpenFlexure Simple Illumination

This was a test project to create a simple illumination board. It has never been implemented. Active development of an illumination board for the OpenFlexure project has moved to [a smaller constant current board](https://gitlab.com/openflexure/openflexure-constant-current-illumination).

![](https://gitlab.com/openflexure/openflexure-simple-illumination/-/raw/master/Render_front.png)  
![](https://gitlab.com/openflexure/openflexure-simple-illumination/-/raw/master/Render_back.png)  
Note the LED on the final board should be white not red!
